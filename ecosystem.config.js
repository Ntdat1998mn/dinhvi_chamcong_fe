module.exports = {
  apps: [
    {
      name: "dinhvi_chamcong_3005",
      script: "serve", // Sử dụng tool serve để phục vụ tệp tĩnh
      env: {
        PM2_SERVE_PATH: "./build",
        PM2_SERVE_PORT: 3005, // Cổng HTTPS
        PM2_SERVE_SPA: "true",
        PM2_SERVE_SSL_KEY: "/etc/pki/tls/private/ca.key.pem",
        PM2_SERVE_SSL_CRT: "/etc/pki/tls/certs/ca.crt.pem",
      },
    },
  ],
};
