import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import LoginPage from "./Pages/LoginPage/LoginPage";
import HomePage from "./Pages/HomePage/HomePage";
import Layout from "./HOC/Layout/Layout";
import LichCongTacPage from "./Pages/LichCongTacPage/LichCongTacPage";
import SubLayout from "./HOC/Layout/SubLayout";
import OutTimePage from "./Pages/OutTimePage/OutTimePage";
import { useSelector } from "react-redux";
import NghiPhepPage from "./Pages/NghiPhepPage/NghiPhepPage";
import LichLamViecPage from "./Pages/LichLamViecPage/LichLamViecPage";
import ProfilePage from "./Pages/ProfilePage/ProfilePage";
import ChangePasswordPage from "./Pages/ChangePasswordPage/ChangePasswordPage";

function App() {
  let isModal = useSelector((state) => state.modalSlice.isModal);
  return (
    <>
      {isModal && (
        <div className="w-screen h-screen absolute top-0 left-0 bg-black opacity-40 z-40"></div>
      )}
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<LoginPage />} />
          <Route path="/" element={<Layout Component={HomePage} />} />
          <Route
            path="/lich-cong-tac/:tab"
            element={
              <SubLayout Component={LichCongTacPage} title="LỊCH CÔNG TÁC" />
            }
          />
          <Route
            path="/thong-tin-ca-nhan"
            element={
              <SubLayout Component={ProfilePage} title="THÔNG TIN CÁ NHÂN" />
            }
          />
          <Route
            path="/doi-mat-khau"
            element={
              <SubLayout Component={ChangePasswordPage} title="ĐỔI MẬT KHẨU" />
            }
          />
          <Route
            path="/lich-lam-viec/:tab"
            element={
              <SubLayout Component={LichLamViecPage} title="LỊCH LÀM VIỆC" />
            }
          />
          <Route
            path="/nghi-phep/:tab"
            element={<SubLayout Component={NghiPhepPage} title="NGHỈ PHÉP" />}
          />
          <Route
            path="/tang-ca/:tab"
            element={<SubLayout Component={OutTimePage} title="TĂNG CA" />}
          />
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
