import React from "react";
import { starSVG, stopWatchSVG } from "../../issets/img/svg";
import { useSelector, useDispatch } from "react-redux";
import { setMainTab } from "../../reduxToolkit/reducer/tabSlice";
import { useNavigate } from "react-router-dom";

export default function Footer() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const mainTab = useSelector((state) => state.tabSlice.mainTab);
  const handleChangeMainTab = (mainTab) => {
    dispatch(setMainTab(mainTab));
    navigate("/");
  };
  return (
    <footer className="bg-white absolute flex bottom-0 left-0 w-full text-gray-500 fill-gray-500 h-24">
      <button
        onClick={() => {
          handleChangeMainTab("cham-cong");
        }}
        className={`flex flex-col items-center w-1/2 h-full py-4 ${
          mainTab == "cham-cong" && "fill-sky-500 text-sky-500"
        }`}
      >
        <div className="w-10">{stopWatchSVG}</div>
        <p>Chấm công</p>
      </button>
      <button
        onClick={() => {
          handleChangeMainTab("cong-viec");
        }}
        className={`flex flex-col items-center w-1/2 h-full py-4 ${
          mainTab == "cong-viec" && "fill-sky-500 text-sky-500"
        }`}
      >
        <div className="w-10">{starSVG}</div>
        <p>Công việc</p>
      </button>
    </footer>
  );
}
