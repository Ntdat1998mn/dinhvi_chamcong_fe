import React, { useEffect } from "react";
import avatar from "../../issets/img/avatar.jpg";
import { menuSVG } from "../../issets/img/svg";
import { useNavigate } from "react-router";
import { useDispatch, useSelector } from "react-redux";
import { setIsMenu } from "../../reduxToolkit/reducer/modalSlice";
import { BASE_URL } from "../../services/configURL";

export default function Header() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const user = useSelector((state) => state.userSlice.userInfor);
  return (
    <header className="p-4 text-white relative">
      <div className="flex justify-between">
        <div className="flex items-center">
          <div className="w-16 h-16 rounded-full overflow-hidden">
            <img
              onClick={() => {
                navigate("/thong-tin-ca-nhan");
              }}
              className="w-full h-full object-cover cursor-pointer"
              src={user.nv_avatar ? `${BASE_URL + user.nv_avatar}` : avatar}
            />
          </div>
          <div className="ml-2">
            <h2 className="font-medium text-lg uppercase">{user.nv_name}</h2>
            <span className="mr-2">Chức vụ:</span>
            <span className="uppercase">{user.dvcc_chuc_vu?.chuc_vu_name}</span>
          </div>
        </div>
        <div
          onClick={() => {
            dispatch(setIsMenu(true));
          }}
          className="fill-white cursor-pointer"
        >
          {menuSVG}
        </div>
      </div>
    </header>
  );
}
