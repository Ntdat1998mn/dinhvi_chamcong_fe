import React from "react";
import { NavLink } from "react-router-dom";
import { angleLeftSVG } from "../../issets/img/svg";
import { useDispatch } from "react-redux";
import { setIsMenu } from "../../reduxToolkit/reducer/modalSlice";

export default function SubHeader({ title }) {
  let dispatch = useDispatch();
  return (
    <div className="flex justify-center items-center relative text-center text-white text-2xl font-medium bg-gradient-to-b from-sky-500 to-blue-800 h-20 rounded-b-xl">
      <NavLink
        to={"/"}
        onClick={() => {
          dispatch(setIsMenu(0));
        }}
        activeclass="active"
        className="fill-white absolute top-1/2 -translate-x-1/2 -translate-y-1/2 left-8"
      >
        {angleLeftSVG}
      </NavLink>
      <p>{title}</p>
    </div>
  );
}
