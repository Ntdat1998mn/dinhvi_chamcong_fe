import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  setIsValid,
  setPosition,
} from "../../reduxToolkit/reducer/locationSlice";

export default function LiveDistance() {
  const branch = useSelector(
    (state) => state.userSlice.userInfor.dvcc_chi_nhanh
  );

  let dispatch = useDispatch();
  const calculateDistance = (lat1, lon1, lat2, lon2) => {
    const R = 6371;
    const dLat = (lat2 - lat1) * (Math.PI / 180);
    const dLon = (lon2 - lon1) * (Math.PI / 180);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(lat1 * (Math.PI / 180)) *
        Math.cos(lat2 * (Math.PI / 180)) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const distance = R * c;
    return distance;
  };
  let branchLocation = branch;

  const [distance, setDistance] = useState(999);
  useEffect(() => {
    const watchId = navigator.geolocation.watchPosition(
      (position) => {
        dispatch(
          setPosition({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
          })
        );
        if (branchLocation) {
          const newDistance = calculateDistance(
            branchLocation.latitude,
            branchLocation.longitude,
            position.coords.latitude,
            position.coords.longitude
          );
          if (newDistance <= 2) {
            dispatch(setIsValid(true));
          } else dispatch(setIsValid(false));
          setDistance(newDistance.toFixed(2));
        }
      },
      (err) => {
        console.log(err);
      },
      { enableHighAccuracy: true }
    );

    return () => {
      navigator.geolocation.clearWatch(watchId);
    };
  }, []);

  return <span>{distance}</span>;
}
