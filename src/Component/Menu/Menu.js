import React from "react";
import { defendSVG, inforSVG, lockSVG, powerSVG } from "../../issets/img/svg";
import avatar from "../../issets/img/avatar.jpg";
import { useNavigate } from "react-router";
import { sessionService } from "../../services/sessionService";
import { BASE_URL, DINHVI_CHAMCONG_USER } from "../../services/configURL";
import { useDispatch, useSelector } from "react-redux";
import { setIsMenu } from "../../reduxToolkit/reducer/modalSlice";
import logo from "../../issets/img/logo-profile.jpg";

export default function Menu({ isMenu }) {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const user = useSelector((state) => state.userSlice.userInfor);
  const handleLogout = () => {
    sessionService.removeItem(DINHVI_CHAMCONG_USER);
    dispatch(setIsMenu(false));
    navigate("/login");
  };

  return (
    <div
      className={`absolute top-0 left-0 w-1/2 h-screen bg-white shadow-lg z-40 -translate-x-full opacity-0 ${
        isMenu ? "animation_leftIn" : isMenu !== 0 ? "animation_rightIn" : ""
      }`}
    >
      <div className="w-full h-full relative">
        <div
          style={{ backgroundImage: `url(${logo})` }}
          className="h-20 bg-sky-500 bg-cover bg-center shadow"
        ></div>
        <div className="py-4 px-1 flex flex-col items-center">
          <div className="w-16 h-16 rounded-full overflow-hidden">
            <img
              className="object-cover w-full h-full"
              src={user.nv_avatar ? `${BASE_URL + user.nv_avatar}` : avatar}
            />
          </div>
          <div className="text-center mt-4">
            <h2 className="text-sky-500 font-medium text-lg uppercase">
              {user.nv_name}
            </h2>
            <p className="text-gray-500 border-b pb-2 mb-2 border-gray-500">
              <span>Chức vụ: </span>{" "}
              <span>{user.dvcc_chuc_vu?.chuc_vu_name}</span>
            </p>
          </div>
          <div className="text-sky-500 w-full text-left">
            <button
              onClick={() => {
                navigate("/doi-mat-khau");
              }}
              className="flex items-center leading-7"
            >
              <span className="mr-2 fill-sky-500">{lockSVG}</span>
              <span>Đổi mật khẩu</span>
            </button>
            <button className="flex items-center leading-7">
              <span className="mr-2 fill-sky-500">{defendSVG}</span>
              <span>Chính sách bảo mật</span>
            </button>
            <button className="flex items-center leading-7">
              <span className="mr-2 fill-sky-500">{inforSVG}</span>
              <span>Phiên bảo 1.1.1</span>
            </button>
          </div>
        </div>
        <div className="flex justify-center absolute h-10 active:bg-gray-400 bg-red-500 text-white bottom-0 w-full">
          <span className="fill-white stroke-white flex items-center">
            {powerSVG}
          </span>
          <button
            onClick={handleLogout}
            className="font-medium ml-3 leading-10"
          >
            ĐĂNG XUẤT
          </button>
        </div>
      </div>
    </div>
  );
}
