import React, { useState } from "react";
import { userService } from "../../services/userService";
import { useDispatch, useSelector } from "react-redux";
import { showMessage } from "../../utils/commonHandler";
import { setUserInfor } from "../../reduxToolkit/reducer/userSlice";

export default function ChangeAvatarModel({ setShowAvatarModal }) {
  let dispatch = useDispatch();
  const user = useSelector((state) => state.userSlice.userInfor);
  let [avatar, setAvatar] = useState({
    selectedAvatar: null,
    selectedAvatarToShow: null,
  });
  const handleImageChange = (event) => {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      const selectedAvatarToShow = reader.result;
      const selectedAvatar = file;
      setAvatar({
        selectedAvatarToShow,
        selectedAvatar,
      });
    };
    if (file) {
      reader.readAsDataURL(file);
    }
  };
  const handleCloseModal = () => {
    setAvatar({
      selectedAvatar: null,
      selectedAvatarToShow: null,
    });
    setShowAvatarModal(false);
  };
  const handleUpdate = () => {
    userService
      .uploadAvatar(avatar.selectedAvatar, user.token)
      .then((res) => {
        showMessage("Cập nhật ảnh đại diện thành công!", "success");
        dispatch(setUserInfor(res.data.content));
        handleCloseModal();
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  };
  return (
    <div className="absolute w-full h-screen top-0 left-0">
      <div className="opacity-10 bg-black w-screen h-screen top-0 left-0 z-40 absolute"></div>
      <div className="absolute top-1/2 left-1/2 p-4 rounded shadow -translate-x-1/2 -translate-y-1/2 flex flex-col justify-center bg-white  items-center mx-auto z-50 min-w-[300px]">
        <h3 className="font-medium text-lg uppercase text-black text-center w-full mb-3">
          Cập nhật Avatar
        </h3>
        {avatar.selectedAvatarToShow && (
          <div className="w-36 h-36 p-1 border-2 border-sky-500 rounded-full">
            <img
              className="h-full w-full rounded-full object-cover"
              src={avatar.selectedAvatarToShow}
            />
          </div>
        )}
        <div className="mt-3 flex justify-between text-center w-full flex-wrap">
          <input
            className="hidden"
            id="input"
            type="file"
            accept="image/*"
            capture="camera"
            onChange={handleImageChange}
          />
          <label
            htmlFor="input"
            className="w-5/12 bg-green-500 text-white rounded py-1"
          >
            Chọn ảnh
          </label>
          <button
            onClick={handleUpdate}
            className="w-5/12 bg-sky-500 text-white rounded py-1 active:bg-gray-300"
          >
            Cập nhật
          </button>
          <button
            onClick={handleCloseModal}
            className="w-full bg-red-500 text-white rounded py-1 active:bg-gray-300 mt-2"
          >
            Huỷ
          </button>
        </div>
      </div>
    </div>
  );
}
