import React, { useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import { userService } from "../../services/userService";
import { setUserInfor } from "../../reduxToolkit/reducer/userSlice";
import { showMessage } from "../../utils/commonHandler";

export default function UpdateUserInforModal({ setShowUpdateUserInforModal }) {
  const user = useSelector((state) => state.userSlice.userInfor);

  let phoneRef = useRef(null);
  let emailRef = useRef(null);
  let ngaySinhRef = useRef(null);
  let dcThuongTruRef = useRef(null);
  let dcLienHeRef = useRef(null);
  const handleConfirm = () => {
    const nv_sdt_lienhe = phoneRef.current.value;
    const nv_ngaysinh = ngaySinhRef.current.value;
    const nv_email = emailRef.current.value;
    const nv_diachithuongtru = dcThuongTruRef.current.value;
    const nv_diachitamtru = dcLienHeRef.current.value;
    userService
      .updateUserInfor(
        {
          nv_diachitamtru,
          nv_diachithuongtru,
          nv_email,
          nv_ngaysinh,
          nv_sdt_lienhe,
        },
        user.token
      )
      .then((res) => {
        setUserInfor(res.data.content);
        showMessage(res.data.message, "success");
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  };
  useEffect(() => {
    if (phoneRef.current) {
      phoneRef.current.value = user.nv_sdt_lienhe;
    }
    if (user.nv_email) {
      emailRef.current.value = user.nv_email;
    }
    if (user.nv_ngaysinh) {
      ngaySinhRef.current.value = user.nv_ngaysinh;
    }
    if (user.nv_diachithuongtru) {
      dcThuongTruRef.current.value = user.nv_diachithuongtru;
    }
    if (user.nv_diachitamtru) {
      dcLienHeRef.current.value = user.nv_diachitamtru;
    }
  }, []);

  return (
    <div className="absolute w-screen h-screen top-0 left-0">
      <div className="opacity-10 bg-black w-screen h-screen top-0 left-0 z-40 absolute"></div>
      <div className="absolute top-1/2 left-1/2 p-4 rounded shadow -translate-x-1/2 -translate-y-1/2 flex flex-col justify-center bg-white  items-center mx-auto z-50 min-w-[300px]">
        <h3 className="font-medium text-lg uppercase text-black text-center w-full">
          Cập nhật thông tin
        </h3>
        <div className="w-full p-4">
          <div className="border-b border-sky-500">
            <p className="text-black">Số điện thoại:</p>
            <input
              ref={phoneRef}
              className="w-full"
              type="number"
              placeholder="*Số điện thoại"
            />
          </div>
          <div className="border-b border-sky-500">
            <p className="text-black">Emai:</p>
            <input
              ref={emailRef}
              className="w-full"
              type="text"
              placeholder="*Email"
            />
          </div>
          <div className="border-b border-sky-500">
            <p className="text-black">Ngày sinh:</p>
            <input ref={ngaySinhRef} className="w-full" type="date" />
          </div>
          <div className="border-b border-sky-500">
            <p className="text-black">Địa chỉ liên hệ:</p>
            <input
              ref={dcLienHeRef}
              className="w-full"
              type="text"
              placeholder="*Địa chỉ liên hệ"
            />
          </div>
          <div className="border-b border-sky-500">
            <p className="text-black">Địa chỉ thường trú:</p>
            <input
              ref={dcThuongTruRef}
              className="w-full"
              type="text"
              placeholder="*Địa chỉ thường trú"
            />
          </div>
          <div className="flex justify-between">
            <button
              onClick={handleConfirm}
              className="text-white w-5/12 rounded-full mt-6 py-1 bg-gradient-to-b from-sky-400 to-blue-900"
            >
              XÁC NHẬN
            </button>
            <button
              onClick={() => {
                setShowUpdateUserInforModal(false);
              }}
              className="text-white w-5/12 rounded-full mt-6 py-1 bg-red-500"
            >
              HUỶ
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
