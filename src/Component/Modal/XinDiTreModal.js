import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setIsXinDiTreModal } from "../../reduxToolkit/reducer/modalSlice";
import { shiftService } from "../../services/shiftService";
import { showMessage } from "../../utils/commonHandler";
import { userService } from "../../services/userService";

export default function XinDiTreModal() {
  let dispatch = useDispatch();
  const user = useSelector((state) => state.userSlice.userInfor);

  const dateRef = useRef(null);
  const timeToWorkLateRef = useRef(null);
  const reasonRef = useRef(null);
  const dirManagerRef = useRef(null);
  const approverRef = useRef(null);
  const [aproverList, setApoverList] = useState([]);
  const [leaderList, setLeaderList] = useState([]);
  const renderOptionAprover = (userList) => {
    return userList.map((userList, index) => {
      return (
        <option key={index} value={userList.nv_id}>
          {userList.nv_name}
        </option>
      );
    });
  };

  const handleCancelModal = () => {
    dispatch(setIsXinDiTreModal(false));
    resetForm();
  };
  const resetForm = () => {
    dateRef.current.value = "";
    timeToWorkLateRef.current.value = "";
    reasonRef.current.value = "";
  };

  const handleConfirm = () => {
    const ngay = dateRef.current.value;
    const gio_di_tre = timeToWorkLateRef.current.value;
    const ly_do = reasonRef.current.value;

    const data = {
      ngay,
      gio_di_tre,
      ly_do,
    };
    shiftService
      .xinDiTre(data, user.token)
      .then((res) => {
        showMessage(res.data.message, "success");
        handleCancelModal();
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  };
  useEffect(() => {
    userService
      .layNguoiDungBanLanhDaoCongTy(user.token)
      .then((res) => {
        setApoverList(res.data.content);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
    userService
      .layNguoiDungCungPhongBan(user.danhmuc_id, user.token)
      .then((res) => {
        setLeaderList(res.data.content);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  }, []);
  return (
    <div className="min-w-max absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 bg-white z-50 p-4 rounded shadow-white shadow">
      <div className="w-56">
        <h6 className="text-sky-500 font-bold text-center mb-3">Xin đi trễ</h6>
        <div className="w-full h-0.5 bg-gray-400"></div>
        <div>
          <p className="my-2">Ngày:</p>
          <input
            ref={dateRef}
            type="date"
            className="outline-1 w-full outline rounded outline-gray-300 focus:outline-sky-500 focus:outline-1 bg-gray-100"
          />
        </div>
        <div>
          <p className="mt-4">Thời gian vào làm:</p>
          <input
            ref={timeToWorkLateRef}
            type="time"
            className="outline-1 w-full outline rounded outline-gray-300 focus:outline-sky-500 focus:outline-1 bg-gray-100"
          />
        </div>
        <div>
          <p className="mt-4">Quản lý trực tiếp:</p>
          <select
            ref={dirManagerRef}
            className="outline-1 w-full outline-gray-400 outline rounded px-2 focus:outline-sky-500"
          >
            <option value="">---Chọn---</option>{" "}
            {renderOptionAprover(leaderList)}
          </select>
        </div>
        <div>
          <p className="mt-4"> Người duyệt:</p>
          <select
            ref={approverRef}
            className="outline-1 w-full outline-gray-400 outline rounded px-2 focus:outline-sky-500"
          >
            <option value="">---Chọn---</option>
            {renderOptionAprover(aproverList)}
          </select>
        </div>
        <div className="mt-4">
          <p className="mb-2">Lý do:</p>
          <textarea
            ref={reasonRef}
            className="w-full outline-1 outline rounded outline-gray-300 focus:outline-sky-500 focus:outline-1 bg-gray-100"
            rows={4}
          ></textarea>
        </div>
        <div className="flex justify-between mt-2">
          <button
            onClick={handleCancelModal}
            className="bg-gray-600 text-white rounded py-1 px-4 active:bg-red-500"
          >
            HUỶ
          </button>
          <button
            onClick={handleConfirm}
            className="bg-sky-500 text-white rounded py-1 px-4 active:bg-lime-500"
          >
            XÁC NHẬN
          </button>
        </div>
      </div>
    </div>
  );
}
