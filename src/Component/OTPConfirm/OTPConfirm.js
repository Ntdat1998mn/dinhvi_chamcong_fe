import React from "react";

export default function OTPConfirm() {
  return (
    <div className="bg-gray-100 p-6 text-center absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2">
      <h2 className="mt-3 text-sky-500 text-xl font-bold">XÁC MINH OTP</h2>
      <p className="text-sky-500">Nhập mã xác minh tài khoản</p>
      <div className="flex mt-3">
        <input
          className="p-2 outline mx-1 outline-1 outline-gray-400 w-6 rounded"
          type="text"
          inputmode="numeric"
          pattern="[0-9]*"
          maxlength="1"
        />
        <input
          className="p-2 outline mx-1 outline-1 outline-gray-400 w-6 rounded"
          type="text"
          inputmode="numeric"
          pattern="[0-9]*"
          maxlength="1"
        />
        <input
          className="p-2 outline mx-1 outline-1 outline-gray-400 w-6 rounded"
          type="text"
          inputmode="numeric"
          pattern="[0-9]*"
          maxlength="1"
        />
        <input
          className="p-2 outline mx-1 outline-1 outline-gray-400 w-6 rounded"
          type="text"
          inputmode="numeric"
          pattern="[0-9]*"
          maxlength="1"
        />
        <input
          className="p-2 outline mx-1 outline-1 outline-gray-400 w-6 rounded"
          type="text"
          inputmode="numeric"
          pattern="[0-9]*"
          maxlength="1"
        />
        <input
          className="p-2 outline mx-1 outline-1 outline-gray-400 w-6 rounded"
          type="text"
          inputmode="numeric"
          pattern="[0-9]*"
          maxlength="1"
        />
      </div>
      <div className="text-right mt-1">
        <span className="italic text-green-400 underline">Gửi lại mã</span>
      </div>
      <button className="mt-3 text-white w-32 py-1 rounded-lg bg-gradient-to-r from-cyan-400 to-blue-800 active:to-cyan-400 active:from-blue-800">
        XÁC NHẬN
      </button>
    </div>
  );
}
