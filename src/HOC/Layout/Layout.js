import React from "react";
import Header from "../../Component/Header/Header";
import Footer from "../../Component/Footer/Footer";
import XinVeSomModal from "../../Component/Modal/XinVeSomModal";
import { useSelector } from "react-redux";
import XinDiTreModal from "../../Component/Modal/XinDiTreModal";
export default function Layout({ Component }) {
  const user = useSelector((state) => state.userSlice.userInfor);
  if (!user) {
    window.location.href = "/login";
  }

  let isXinDiTreModal = useSelector(
    (state) => state.modalSlice.isXinDiTreModal
  );
  let isXinVeSomModal = useSelector(
    (state) => state.modalSlice.isXinVeSomModal
  );
  return (
    <div className="h-screen w-full bg-gray-100 relative">
      <div className="z-0 bg-gradient-to-b from-cyan-400 to-blue-800 absolute h-1/5 rounded-b-xl w-full top-0 left-0"></div>
      <Header />
      <Component />
      <Footer />
      {isXinDiTreModal && <XinDiTreModal />}
      {isXinVeSomModal && <XinVeSomModal />}
    </div>
  );
}
