import React from "react";
import Footer from "../../Component/Footer/Footer";
import SubHeader from "../../Component/Header/SubHeader";

export default function SubLayout({ Component, title }) {
  return (
    <div className="h-screen w-full bg-gray-100 relative flex flex-col">
      <SubHeader title={title} />
      <Component />
      {title !== "THÔNG TIN CÁ NHÂN" && "ĐỔI MẬT KHẨU" && <Footer />}
    </div>
  );
}
