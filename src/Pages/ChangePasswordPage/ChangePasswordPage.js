import React, { useRef } from "react";
import { useSelector } from "react-redux";
import { showMessage } from "../../utils/commonHandler";
import { userService } from "../../services/userService";

export default function ChangePasswordPage() {
  const user = useSelector((state) => state.userSlice.userInfor);

  let oldPasswordRef = useRef(null);
  let newPasswordRef = useRef(null);
  let rePasswordRef = useRef(null);

  const handleConfirm = () => {
    const oldPassword = oldPasswordRef.current.value;
    const newPassword = newPasswordRef.current.value;
    const rePassword = rePasswordRef.current.value;
    if (newPassword !== rePassword) {
      showMessage("Mật khẩu mới không trùng nhau!", "error");
      return;
    }
    userService
      .updateUserInfor({ oldPassword, newPassword }, user.token)
      .then((res) => {
        showMessage("Đổi mật khẩu thành công!", "success");
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  };
  return (
    <div>
      <div className="bg-gray-300 p-4">
        <p className="mb-2 font-medium">Chú ý: *</p>
        <p>
          Mật khẩu mới không được trùng với mật khẩu cũ <br />
          Mật khẩu phải có ít nhất 8 ký tự <br />
          Mật khẩu phải có ít nhất một chữ, số và ký tự đặt biệt
        </p>
      </div>
      <div className="w-full p-4 mt-3">
        <div className="w-full">
          <p>Mật khẩu hiện tại:</p>
          <input
            ref={oldPasswordRef}
            className="w-full py-1 bg-transparent focus:outline-none border-b border-gray-500 focus:border-sky-500"
            type="password"
            placeholder="Nhập mật khẩu hiện tại"
          />
        </div>
        <div className="w-full mt-6">
          <p>Mật khẩu mới:</p>
          <input
            ref={newPasswordRef}
            className="w-full py-1 bg-transparent focus:outline-none border-b border-gray-500 focus:border-sky-500"
            type="password"
            placeholder="Nhập mật khẩu mới"
          />
          <input
            ref={rePasswordRef}
            className="w-full py-1 bg-transparent focus:outline-none border-b border-gray-500 focus:border-sky-500"
            type="password"
            placeholder="Nhập lại mật khẩu mới"
          />
        </div>
        <button
          onClick={handleConfirm}
          className="text-white rounded-full mt-6 py-1 w-full bg-gradient-to-b from-sky-400 to-blue-900"
        >
          CẬP NHẬT
        </button>
      </div>
    </div>
  );
}
