import React from "react";
import WorkTab from "./WorkTab";
import { useDispatch, useSelector } from "react-redux";
import Menu from "../../Component/Menu/Menu";
import { setIsMenu } from "../../reduxToolkit/reducer/modalSlice";
import Timekeeping from "./Timekeeping";

export default function HomePage() {
  let dispatch = useDispatch();
  const isMenu = useSelector((state) => state.modalSlice.isMenu);
  const mainTab = useSelector((state) => state.tabSlice.mainTab);

  return (
    <>
      {mainTab === "cham-cong" ? <Timekeeping /> : <WorkTab />}
      <Menu isMenu={isMenu} />
      {isMenu ? (
        <div
          onClick={() => {
            dispatch(setIsMenu(false));
          }}
          className="absolute top-0 left-0 h-screen w-full bg-black opacity-30 z-30"
        ></div>
      ) : (
        ""
      )}
    </>
  );
}
