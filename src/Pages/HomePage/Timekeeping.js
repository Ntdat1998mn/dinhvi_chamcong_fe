import React, { useEffect } from "react";
import {
  checkSVG,
  closeSVG,
  locationSVG,
  watchSVG,
} from "../../issets/img/svg";
import LiveClock from "../../Component/LiveClock/LiveClock";
import LiveDistance from "../../Component/LiveDistance/LiveDistance";
import { useSelector } from "react-redux";
import { formatDate, showMessage } from "../../utils/commonHandler";
import { shiftService } from "../../services/shiftService";

export default function Timekeeping() {
  const position = useSelector((state) => state.locationSlice.position);
  const isValid = useSelector((state) => state.locationSlice.isValid);
  const user = useSelector((state) => state.userSlice.userInfor);

  let currentTime = new Date();
  let formatCurrentTime = formatDate(currentTime);
  const handleCheckIn = () => {
    if (isValid) {
      shiftService
        .checkIn(user.token)
        .then((res) => {
          showMessage(res.data.message, "success");
        })
        .catch((err) => {
          showMessage(err.response.data.message, "error");
        });
    } else {
      showMessage("Bạn cần đến địa điểm hợp lệ để thực hiện chấm công!");
    }
  };
  const handleCheckOut = () => {
    if (isValid) {
      shiftService
        .checkOut(user.token)
        .then((res) => {
          showMessage(res.data.message, "success");
        })
        .catch((err) => {
          showMessage(err.response.data.message, "error");
        });
    } else {
      showMessage("Bạn cần đến địa điểm hợp lệ để thực hiện chấm công!");
    }
  };
  return (
    <div className="timekeeping_content w-full relative flex flex-col">
      <div className="border border-b-0 border-gray-400 h-full mx-6 rounded-t-xl overflow-hidden">
        <iframe
          src={`https://maps.google.com/maps?q=${position.latitude},${position.longitude}&output=embed&z=13`}
          width={"100%"}
          height={"100%"}
          allowfullscreen=""
          loading="lazy"
          referrerpolicy="no-referrer-when-downgrade"
        ></iframe>
      </div>
      <div className="bg-white w-full flex items-center px-2 py-4 z-20">
        <div className="flex-1">
          <div className="flex mb-3">
            <div className="flex stroke-sky-500">{watchSVG}</div>
            <div className=" ml-2">
              <p className="text-sky-500 font-medium leading-4">
                {" "}
                <LiveClock />
              </p>
              <p className="text-gray-500">{formatCurrentTime}</p>
            </div>
          </div>
          <div className="flex mb-3">
            <div className="flex fill-red-500">{locationSVG}</div>
            <div className="ml-2">
              <p className="text-sky-500 leading-4">
                <LiveDistance /> km cách{" "}
                <span className="text-gray-500 font-medium">
                  {user.dvcc_chi_nhanh.chi_nhanh_name}
                </span>
              </p>
              <p className="text-gray-500">
                {user.dvcc_chi_nhanh.chi_nhanh_address}
              </p>
            </div>
          </div>
          {isValid ? (
            <div className="fill-lime-500 text-lime-500 font-medium flex items-center">
              <span className="mr-2">{checkSVG}</span>
              <span>Địa điểm hợp lệ</span>
            </div>
          ) : (
            <div className="fill-red-500 text-red-500 font-medium flex items-center">
              <span className="mr-2">{closeSVG}</span>
              <span>Địa điểm không hợp lệ</span>
            </div>
          )}
        </div>
        <div className="flex flex-col justify-center text-white w-24 ml-1">
          <button
            onClick={handleCheckIn}
            className="rounded-full font-medium py-1 px-2 bg-gradient-to-r from-cyan-300 to-blue-800 active:to-cyan-300 active:from-blue-800 mb-3"
          >
            Vào làm
          </button>
          <button
            onClick={handleCheckOut}
            className="rounded-full font-medium py-1 px-2 bg-gradient-to-r to-yellow-300 from-orange-600 active:from-yellow-300 active:to-orange-600"
          >
            Ra về
          </button>
        </div>
      </div>
    </div>
  );
}
