import React from "react";
import { useDispatch } from "react-redux";
import xinDiTre from "../../issets/img/xinditre.jpg";
import xinVeSom from "../../issets/img/xinvesom.jpg";
import tangCa from "../../issets/img/tang-ca.png";
import nghiPhep from "../../issets/img/nghi-phep.png";
import dangKyLichLam from "../../issets/img/dang-ky-lich-lam.png";
import lichCongTac from "../../issets/img/lich-cong-tac.png";
import { NavLink } from "react-router-dom";
import {
  setIsXinDiTreModal,
  setIsXinVeSomModal,
} from "../../reduxToolkit/reducer/modalSlice";

export default function WorkTab() {
  let dispatch = useDispatch();
  const handleXinDiTreModal = () => {
    dispatch(setIsXinDiTreModal(true));
  };
  const handleXinVeSomModal = () => {
    dispatch(setIsXinVeSomModal(true));
  };
  return (
    <div className="relative mx-4">
      <div className="bg-white flex rounded-2xl py-2">
        <div className="text-center w-1/2">
          <p className="text-sky-500 text-2xl font-bold">4</p>
          <p className="text-gray-500">
            Ngày nghỉ <br /> có lương
          </p>
        </div>
        <div className="text-center w-1/2">
          <p className="text-sky-500 text-2xl font-bold">0</p>
          <p className="text-gray-500">
            Ngày nghỉ <br />
            không lương
          </p>
        </div>
      </div>
      <div className="content">
        <div className="mt-2 flex justify-between">
          <button onClick={handleXinDiTreModal} className="pr-3">
            <img src={xinDiTre} alt="Xin đi trễ" />
          </button>
          <button onClick={handleXinVeSomModal} className="pl-3">
            <img src={xinVeSom} alt="Xin đi trễ" />
          </button>
        </div>
        <div className="py-4 text-gray-500 flex flex-col">
          <NavLink
            to="/lich-cong-tac/lich-cong-tac"
            activeclass="active"
            className="my-3"
          >
            <div className="border-l-4 border-sky-500 py-4 rounded bg-white flex items-center">
              <span className="w-10 mx-5">
                <img src={lichCongTac} alt="" />
              </span>
              <span>Lịch công tác</span>
            </div>
          </NavLink>
          <NavLink
            to="/lich-lam-viec/lich-duoc-xep"
            activeclass="active"
            className="my-3"
          >
            <div className="border-l-4 border-lime-500 py-4 rounded bg-white flex items-center">
              <span className="w-10 mx-5">
                <img src={dangKyLichLam} alt="" />
              </span>
              <span>Lịch làm việc (part-time) </span>
            </div>
          </NavLink>
          <NavLink
            to="/tang-ca/lich-tang-ca"
            activeclass="active"
            className="my-3"
          >
            <div className="border-l-4 border-yellow-300 py-4 rounded bg-white flex items-center">
              <span className="w-10 mx-5">
                <img src={tangCa} alt="" />
              </span>
              <span>Tăng ca</span>
            </div>
          </NavLink>
          <NavLink
            to="/nghi-phep/danh-sach-phep"
            activeclass="active"
            className="my-3"
          >
            <div className="border-l-4 border-red-500 py-4 rounded bg-white flex items-center">
              <span className="w-10 mx-5">
                <img src={nghiPhep} alt="" />
              </span>
              <span>Nghỉ phép</span>
            </div>
          </NavLink>
        </div>
      </div>
    </div>
  );
}
