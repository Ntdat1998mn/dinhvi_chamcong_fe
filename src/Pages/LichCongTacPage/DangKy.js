import React, { useEffect, useRef, useState } from "react";
import { userService } from "../../services/userService";
import { useSelector } from "react-redux";
import { showMessage } from "../../utils/commonHandler";
import { checkEmpty } from "../../utils/validate";
import { lichCongTacService } from "../../services/lichCongTacService";

export default function DangKy() {
  const user = useSelector((state) => state.userSlice.userInfor);
  const [aproverList, setApoverList] = useState([]);
  const [leaderList, setLeaderList] = useState([]);
  const renderOptionAprover = (userList) => {
    return userList.map((userList, index) => {
      return (
        <option key={index} value={userList.nv_id}>
          {userList.nv_name}
        </option>
      );
    });
  };
  const startDateRef = useRef(null);
  const endDateRef = useRef(null);
  const addressRef = useRef(null);
  const approverRef = useRef(null);
  const contentRef = useRef(null);

  const handleFormSubmit = () => {
    const formInput = {
      ngay_bat_dau: startDateRef.current.value,
      ngay_ket_thuc: endDateRef.current.value,
      noi_cong_tac: addressRef.current.value,
      nguoi_duyet: approverRef.current.value,
      noi_dung_cong_tac: contentRef.current.value,
    };
    for (let key in formInput) {
      if (!checkEmpty(formInput[key])) {
        showMessage("Vui lòng nhập đầy đủ thông tin!", "error");
        return;
      }
    }
    lichCongTacService
      .dangKyLichCongTac(formInput, user.token)
      .then((res) => {
        showMessage(res.data.message, "success");
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  };
  useEffect(() => {
    userService
      .layNguoiDungBanLanhDaoCongTy(user.token)
      .then((res) => {
        setApoverList(res.data.content);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
    userService
      .layNguoiDungCungPhongBan(user.danhmuc_id, user.token)
      .then((res) => {
        setLeaderList(res.data.content);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  }, []);
  return (
    <div className="content bg-white px-4 py-6">
      <div className="mb-4">
        <div className="mb-2 flex">
          Ngày bắt đầu:{" "}
          <span className="ml-2 flex-1">
            <input
              ref={startDateRef}
              className="outline-1 w-full outline-gray-400 outline rounded px-2 focus:outline-sky-500"
              type="date"
            />
          </span>
        </div>
      </div>
      <div className="mb-4">
        <div className="mb-2 flex">
          Ngày kết thúc:
          <span className="ml-2 flex-1">
            <input
              ref={endDateRef}
              className="outline-1 w-full outline-gray-400 outline rounded px-2 focus:outline-sky-500"
              type="date"
            />
          </span>
        </div>
      </div>
      <div className="mb-4">
        <div className="mb-2">
          Địa chỉ:
          <div className="w-full mt-2">
            <input
              ref={addressRef}
              className="outline-1 w-full outline-gray-400 outline rounded px-2 focus:outline-sky-500"
              type="text"
            />
          </div>
        </div>
      </div>
      <div className="mb-4">
        <div className="mb-2 flex">
          Quản lý trực tiếp:
          <span className="ml-4 flex-1">
            <select
              ref={approverRef}
              className="outline-1 w-full outline-gray-400 outline rounded px-2 focus:outline-sky-500"
            >
              <option value="">---Chọn---</option>
              {renderOptionAprover(leaderList)}
            </select>
          </span>
        </div>
      </div>
      <div className="mb-4">
        <div className="mb-2 flex">
          Người duyệt:
          <span className="ml-4 flex-1">
            <select
              ref={approverRef}
              className="outline-1 w-full outline-gray-400 outline rounded px-2 focus:outline-sky-500"
            >
              <option value="">---Chọn---</option>
              {renderOptionAprover(aproverList)}
            </select>
          </span>
        </div>
      </div>
      <div>
        <p className="mb-2">Nội dung:</p>
        <textarea
          ref={contentRef}
          rows="4"
          className="w-full outline outline-1 outline-gray-400 rounded"
        ></textarea>
      </div>
      <button
        onClick={handleFormSubmit}
        className="text-white rounded-full mt-6 py-1 w-full bg-gradient-to-b from-sky-400 to-blue-900"
      >
        GỬI YÊU CẦU
      </button>
    </div>
  );
}
