import React, { useEffect, useState } from "react";
import { circleSVG, locationSVG } from "../../issets/img/svg";
import { lichCongTacService } from "../../services/lichCongTacService";
import { showMessage } from "../../utils/commonHandler";
import { useSelector } from "react-redux";

export default function LichCongTac() {
  const user = useSelector((state) => state.userSlice.userInfor);
  const [scheduleList, setScheduleList] = useState([]);
  const huyDonXinCongTac = (id) => {
    lichCongTacService
      .huyDonXinCongTac(id, user.token)
      .then((res) => {
        showMessage(res.data.message, "success");
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  };
  const renderWorkingScheduleList = () => {
    return scheduleList.map((schedule, index) => {
      return (
        <div key={index} className="bg-white rounded-lg py-3 pl-6 mb-4">
          <div className="px-6 relative border-dashed border-blue-500 border-l-2">
            <p>
              Từ {schedule.ngay_bat_dau} đến {schedule.ngay_ket_thuc}
            </p>
            <p className="mt-2">
              {schedule.noi_dung_cong_tac} tại {schedule.noi_cong_tac}
            </p>
            <p className="mt-1">
              {schedule.noi_dung_cong_tac} tại {schedule.noi_cong_tac}
            </p>

            <p className="mt-1">
              {" "}
              Quản lý trực tiếp:{" "}
              <span className="font-medium text-lime-500">
                {schedule.thong_tin_nguoi_quan_ly_truc_tiep?.nv_name}
              </span>
            </p>
            <p className="mt-1">
              {" "}
              Người duyệt:{" "}
              <span className="font-medium text-lime-500">
                {schedule.thong_tin_nguoi_duyet?.nv_name}
              </span>
            </p>
            <p className="mt-1">
              Trạng thái:{" "}
              {schedule.status == 1 && (
                <span className="font-medium text-red-500">Chưa duyệt</span>
              )}
              {schedule.status == 2 && (
                <span className="font-medium text-red-500">Chưa duyệt</span>
              )}
              {schedule.status == 3 && (
                <span className="font-medium text-lime-500">Đã duyệt</span>
              )}
              {schedule.status == 4 && (
                <span className="font-medium text-gray-500">Quá hạn</span>
              )}
            </p>
            {/* <p className="mt-2">{schedule.ngay_ket_thuc}</p> */}
            {schedule.status !== 4 && schedule.status !== 3 && (
              <button
                onClick={() => {
                  huyDonXinCongTac(schedule.id);
                }}
                className="w-full bg-red-500 text-white mt-3 active:bg-gray-400"
              >
                Huỷ
              </button>
            )}
            <span className="absolute -margin-left-1 -top-1 -left-0 -translate-x-1/2 fill-red-500 bg-white">
              {locationSVG}
            </span>
            <span className="absolute -margin-left-1 bottom-0 left-0 -translate-x-1/2 fill-gray-400">
              {circleSVG}
            </span>
          </div>
        </div>
      );
    });
  };
  useEffect(() => {
    lichCongTacService
      .getLichCongTac(user.token)
      .then((res) => {
        setScheduleList(res.data.content);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  }, []);
  return <>{renderWorkingScheduleList()}</>;
}
