import React from "react";
import { useState } from "react";
import { circleSVG, locationSVG } from "../../issets/img/svg";
import { useParams } from "react-router";
import { NavLink } from "react-router-dom";
import LichCongTac from "./LichCongTac";
import DangKy from "./DangKy";

export default function LichCongTacPage() {
  const { tab } = useParams();

  return (
    <div className="text-gray-500">
      {/* Phần điều hướng */}
      <div className="flex justify-between font-medium mt-2 bg-white rounded-full px-10">
        <NavLink
          to="/lich-cong-tac/lich-cong-tac"
          activeclass="active"
          className={`border-b-2 py-2 ${
            tab == "lich-cong-tac"
              ? "border-sky-400 text-sky-500"
              : "border-transparent"
          }`}
        >
          Lịch công tác
        </NavLink>
        <NavLink
          to="/lich-cong-tac/dang-ky"
          activeclass="active"
          className={`border-b-2 py-2 ${
            tab == "dang-ky"
              ? "border-sky-400 text-sky-500"
              : "border-transparent"
          }`}
        >
          Đăng ký
        </NavLink>
      </div>
      {/* Phần nội dung */}
      <div className="content px-4 mt-4">
        {tab === "lich-cong-tac" ? <LichCongTac /> : <DangKy />}
      </div>
    </div>
  );
}
