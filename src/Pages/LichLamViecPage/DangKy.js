import React, { useEffect, useRef, useState } from "react";
import { caLamViecService } from "../../services/caLamViecService";
import { useSelector } from "react-redux";
import {
  formatDate,
  getDayOfNextWeek,
  showMessage,
} from "../../utils/commonHandler";
import { lichLamViecService } from "../../services/lichLamViecService";

export default function DangKy() {
  const user = useSelector((state) => state.userSlice.userInfor);
  const [caList, setCaList] = useState([]);
  const refs = useRef([]);
  const renderOptionCa = () => {
    return caList.map((aprover, index) => {
      return (
        <option key={index} value={aprover.id}>
          {aprover.name}
        </option>
      );
    });
  };
  let dayOfNextWeek = getDayOfNextWeek();
  const handleSubmit = () => {
    const data = dayOfNextWeek.map((day, index) => ({
      ngay: day,
      ca_lam_viec_id: refs.current[index].value,
    }));
    let newData = data.filter((item) => {
      return item.ca_lam_viec_id !== "";
    });
    lichLamViecService
      .dangKyLichLamViec(newData, user.token)
      .then((res) => {
        showMessage(res.data.message, "success");
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  };
  const renderContent = () => {
    return dayOfNextWeek.map((day, index) => {
      return (
        <div key={index} className="p-2 rounded-lg">
          <label className="mr-2" htmlFor="monday">
            {formatDate(new Date(day))}
          </label>
          <select
            ref={(ref) => (refs.current[index] = ref)}
            className="focus:outline-0 mt-1 py-1 w-full bg-sky-500 rounded px-2 text-white text-base"
          >
            {renderOptionCa()}
            <option value="">Không</option>
          </select>
        </div>
      );
    });
  };

  useEffect(() => {
    caLamViecService
      .getCaLamViecList(user.token)
      .then((res) => {
        setCaList(res.data.content);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  }, []);

  return (
    <>
      {/* Phần ghi chú */}
      <div className="bg-white mt-4 p-2 rounded-lg mb-3">
        <p>Vui lòng đăng ký trước 24h thứ 6 hàng tuần</p>
        <p>Ca A: 6h - 14h</p>
        <p>Ca B: 14h - 22h</p>
        <p className="text-red-500">Lưu ý: chỉ áp dụng với part - time</p>
      </div>
      {/* Phần nội dung */}
      <div className="content_dangkylichlam">
        <div className=" bg-white py-2 rounded-lg overflow-auto">
          {renderContent()}
          {/* <div className="p-2 rounded-lg">
            <label className="mr-2" htmlFor="monday">
              Thứ 2: <span className="text-red-500">*</span>
            </label>
            <select className="focus:outline-0 mt-1 w-full bg-sky-500 rounded px-2 text-white text-base">
              {renderOptionCa()}
              <option value="">Không</option>
            </select>
          </div>
          <div className="p-2 rounded-lg">
            <label className="mr-2" htmlFor="monday">
              Thứ 3: <span className="text-red-500">*</span>
            </label>
            <select className="focus:outline-0 mt-1 w-full bg-sky-500 rounded px-2 text-white text-base">
              {renderOptionCa()}
              <option value="">Không</option>
            </select>
          </div>
          <div className="p-2 rounded-lg">
            <label className="mr-2" htmlFor="monday">
              Thứ 4: <span className="text-red-500">*</span>
            </label>
            <select className="focus:outline-0 mt-1 w-full bg-sky-500 rounded px-2 text-white text-base">
              {renderOptionCa()}
              <option value="">Không</option>
            </select>
          </div>
          <div className="p-2 rounded-lg">
            <label className="mr-2" htmlFor="monday">
              Thứ 5: <span className="text-red-500">*</span>
            </label>
            <select className="focus:outline-0 mt-1 w-full bg-sky-500 rounded px-2 text-white text-base">
              {renderOptionCa()}
              <option value="">Không</option>
            </select>
          </div>
          <div className="p-2 rounded-lg">
            <label className="mr-2" htmlFor="monday">
              Thứ 6: <span className="text-red-500">*</span>
            </label>
            <select className="focus:outline-0 mt-1 w-full bg-sky-500 rounded px-2 text-white text-base">
              {renderOptionCa()}
              <option value="">Không</option>
            </select>
          </div>
          <div className="p-2 rounded-lg">
            <label className="mr-2" htmlFor="monday">
              Thứ 7: <span className="text-red-500">*</span>
            </label>
            <select className="focus:outline-0 mt-1 w-full bg-sky-500 rounded px-2 text-white text-base">
              {renderOptionCa()}
              <option value="">Không</option>
            </select>
          </div> */}
        </div>
        <button
          onClick={handleSubmit}
          className="text-white rounded-full my-5 py-1 w-full bg-gradient-to-b from-sky-400 to-blue-900"
        >
          XÁC NHẬN
        </button>
      </div>
    </>
  );
}
