import React, { useEffect, useState } from "react";
import { circleSVG } from "../../issets/img/svg";
import { useSelector } from "react-redux";
import { formatDate, showMessage } from "../../utils/commonHandler";
import { lichLamViecService } from "../../services/lichLamViecService";

export default function LichDuocXep() {
  const user = useSelector((state) => state.userSlice.userInfor);

  const [lichDuocXepList, setLichDuocXepList] = useState([]);
  const renderLichDuocXepList = () => {
    return lichDuocXepList.map((schedule, index) => {
      return (
        <div key={index} className="bg-white rounded-lg py-3 pl-6 mb-4">
          <div className="px-6 relative border-dashed border-blue-500 border-l-2">
            <p>
              <span>{formatDate(new Date(schedule.ngay))}</span>
            </p>
            <p className="mt-2">
              <span>Vào ca: </span>
              <span>{schedule.dvcc_ca_lam_viec.gio_bat_dau}</span>
            </p>
            <p className="mt-2">
              <span>Ra ca: </span>
              <span>{schedule.dvcc_ca_lam_viec.gio_ket_thuc}</span>
            </p>
            <span className="absolute -margin-left-1 -top-1 -left-0 -translate-x-1/2 fill-sky-400 bg-white">
              {circleSVG}
            </span>
            <span className="absolute -margin-left-1 bottom-0 left-0 -translate-x-1/2 fill-gray-400">
              {circleSVG}
            </span>
          </div>
        </div>
      );
    });
  };
  useEffect(() => {
    lichLamViecService
      .getLichLamViecDuocDuyet(user.token)
      .then((res) => {
        setLichDuocXepList(res.data.content);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  }, []);

  return <div className="content mt-4">{renderLichDuocXepList()}</div>;
}
