import React from "react";
import { NavLink, useParams } from "react-router-dom";
import DangKy from "./DangKy";
import LichDuocXep from "./LichDuocXep";
import LichDangKy from "./LichDangKy";

export default function DangKyLichLamPage() {
  const { tab } = useParams();
  return (
    <div className="text-gray-500">
      {/* Phần điều hướng */}
      <div className="flex justify-between font-medium mt-2 bg-white rounded-full px-4">
        <NavLink
          to="/lich-lam-viec/lich-duoc-xep"
          activeclass="active"
          className={`border-b-2 py-2 ${
            tab == "lich-duoc-xep"
              ? "border-sky-400 text-sky-500"
              : "border-transparent"
          }`}
        >
          Lịch được xếp
        </NavLink>
        <NavLink
          to="/lich-lam-viec/lich-dang-ky"
          activeclass="active"
          className={`border-b-2 py-2 ${
            tab == "lich-dang-ky"
              ? "border-sky-400 text-sky-500"
              : "border-transparent"
          }`}
        >
          Lịch đăng ký
        </NavLink>
        <NavLink
          to="/lich-lam-viec/dang-ky"
          activeclass="active"
          className={`border-b-2 py-2 ${
            tab == "dang-ky"
              ? "border-sky-400 text-sky-500"
              : "border-transparent"
          }`}
        >
          Đăng ký
        </NavLink>
      </div>
      {/* Phần nội dung */}
      <div className="px-4">
        {tab === "lich-duoc-xep" ? (
          <LichDuocXep />
        ) : tab === "dang-ky" ? (
          <DangKy />
        ) : (
          <LichDangKy />
        )}
      </div>
    </div>
  );
}
