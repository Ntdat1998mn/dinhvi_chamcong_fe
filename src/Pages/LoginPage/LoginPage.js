import React, { useRef, useState } from "react";
import OTPConfirm from "../../Component/OTPConfirm/OTPConfirm";
import { showMessage } from "../../utils/commonHandler";
import { userService } from "../../services/userService";
import { DINHVI_CHAMCONG_USER } from "../../services/configURL";
import { useDispatch } from "react-redux";
import { setUserInfor } from "../../reduxToolkit/reducer/userSlice";
import { useNavigate } from "react-router";
import { sessionService } from "../../services/sessionService";
import logo from "../../issets/img/logo.jpg";

export default function LoginPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const usernameRef = useRef(null);
  const passwordRef = useRef(null);

  const handleLogin = () => {
    const nv_username = usernameRef.current.value;
    const nv_password = passwordRef.current.value;
    if (!nv_username || !nv_password) {
      showMessage("Vui lòng nhập tên đăng nhập và mật khẩu", "error");
    } else {
      userService
        .login({ nv_username, nv_password })
        .then((res) => {
          showMessage(res.data.message, "success");
          sessionService.setItem(res.data.content, DINHVI_CHAMCONG_USER);
          dispatch(setUserInfor(res.data.content));
          navigate("/");
        })
        .catch((err) => {
          console.log("err: ", err);
          showMessage(err.response.data.message, "error");
        });
    }
  };
  return (
    <>
      <div className="w-full h-screen login_page text-center">
        <div
          style={{ backgroundImage: `url(${logo})` }}
          className="w-full h-1/3 bg-cover bg-center"
        ></div>
        <div className="w-full h-2/3 relative bg-gradient-to-tl to-cyan-300 via-blue-600 from-blue-950">
          <div className="w-full absolute -top-10">
            <div className="w-4/5 rounded-lg bg-gray-100 mx-auto p-6 shadow-xl">
              <h2 className="text-sky-500 text-xl font-bold">ĐĂNG NHẬP</h2>
              <input
                className="w-full p-2 mt-3 rounded-lg outline outline-2 outline-gray-300 focus:outline-blue-400"
                type="text"
                ref={usernameRef}
                placeholder="Tài khoản"
              />
              <input
                className="w-full p-2 mt-3 rounded-lg outline outline-2 outline-gray-300 focus:outline-blue-400"
                type="password"
                ref={passwordRef}
                placeholder="Mật khẩu"
              />
              <div className="flex justify-between italic mt-3">
                <div>
                  <input type="checkbox" />
                  <span className="ml-1">Ghi nhớ tài khoản</span>
                </div>
                <a href="#" className="text-green-500">
                  Quên mật khẩu?
                </a>
              </div>
              <button
                onClick={handleLogin}
                className="mt-3 text-white w-32 py-1 rounded-lg bg-gradient-to-r from-cyan-400 to-blue-800 active:to-cyan-400 active:from-blue-800"
              >
                ĐĂNG NHẬP
              </button>
            </div>
          </div>
        </div>
      </div>
      {false && <OTPConfirm />}
    </>
  );
}
