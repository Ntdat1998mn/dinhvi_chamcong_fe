import React, { useEffect, useState } from "react";
import {
  circleQuestionSVG,
  circleSVG,
  circleUserSVG,
  watchSVG,
} from "../../issets/img/svg";
import { useSelector } from "react-redux";
import { nghiPhepService } from "../../services/nghiPhepService";
import { showMessage } from "../../utils/commonHandler";

export default function DanhSachPhep() {
  const user = useSelector((state) => state.userSlice.userInfor);

  const [danhSachPhep, setDanhSachPhep] = useState([]);
  const huyDonXinNghiPhep = (id) => {
    nghiPhepService
      .huyDonNghiPhep(id, user.token)
      .then((res) => {
        showMessage(res.data.message, "success");
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  };
  const renderDanhSachPhep = () => {
    return danhSachPhep.map((phepNghi, index) => {
      return (
        <div key={index} className="bg-white p-4 rounded mb-3">
          <div className="flex justify-between items-center ">
            <div>
              <div className="flex items-center">
                <span className="fill-gray-400 mr-2">{circleSVG}</span>
                <span className="mr-1">Nghỉ phép: </span>
                <span>3 ngày</span>
              </div>
              <div className="flex items-center">
                <span className="mr-2 stroke-sky-500">{watchSVG}</span>
                <div className="flex flex-col">
                  <div>
                    <span className="mr-1">Từ ngày: </span>
                    <span>{phepNghi.ngay_bat_dau}</span>
                  </div>
                  <div>
                    <span className="mr-1">Đến ngày: </span>
                    <span>{phepNghi.ngay_ket_thuc}</span>
                  </div>
                </div>
              </div>
              <div className="flex items-center">
                <span className="stroke-red-500 mr-2">{circleQuestionSVG}</span>
                <span className="mr-1">
                  Lý do: <span>{phepNghi.ly_do}</span>
                </span>
              </div>
              <div className="flex items-center">
                <span className="fill-lime-500 mr-2">{circleUserSVG}</span>
                <div className="flex flex-col">
                  <div>
                    <span className="mr-1 whitespace-nowrap">
                      Quản lý trực tiếp:
                    </span>
                    <span className="font-medium text-lime-500">
                      {phepNghi.thong_tin_nguoi_quan_ly_truc_tiep?.nv_name}
                    </span>
                  </div>
                  <div>
                    <span className="mr-1 whitespace-nowrap">Người duyệt:</span>
                    <span className="font-medium text-lime-500">
                      {phepNghi.thong_tin_nguoi_duyet?.nv_name}
                    </span>
                  </div>
                </div>
              </div>
            </div>
            {phepNghi.phepnghi_status === 1 && (
              <div>
                <div className="border border-red-500 text-red-500 rounded-full px-4 py-1 text-center">
                  Chưa duyệt
                </div>
              </div>
            )}
            {phepNghi.phepnghi_status === 2 && (
              <div>
                <div className="border border-red-500 text-red-500 rounded-full px-4 py-1 text-center">
                  Chưa duyệt
                </div>
              </div>
            )}
            {phepNghi.phepnghi_status === 3 && (
              <div>
                <div className="border border-lime-500 text-lime-500 rounded-full px-4 py-1 text-center">
                  Đã duyệt
                </div>
              </div>
            )}
            {phepNghi.phepnghi_status === 4 && (
              <div>
                <div className="border border-gray-400 text-gray-400 rounded-full px-4 py-1 text-center">
                  Quá hạn
                </div>
              </div>
            )}
          </div>
          {phepNghi.phepnghi_status !== 4 && phepNghi.phepnghi_status !== 3 && (
            <button
              onClick={() => {
                huyDonXinNghiPhep(phepNghi.id);
              }}
              className="w-full bg-red-500 text-white mt-3 active:bg-gray-400"
            >
              Huỷ
            </button>
          )}
        </div>
      );
    });
  };
  useEffect(() => {
    nghiPhepService
      .getNghiPhep(user.token)
      .then((res) => {
        setDanhSachPhep(res.data.content);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  }, []);
  return <div className="content px-4">{renderDanhSachPhep()}</div>;
}
