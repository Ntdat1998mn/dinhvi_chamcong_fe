import React from "react";
import { NavLink, useParams } from "react-router-dom";
import XinNghiPhep from "./XinNghiPhep";
import DanhSachPhep from "./DanhSachPhep";

export default function NghiPhepPage() {
  const { tab } = useParams();
  return (
    <div className="text-gray-500 h-full">
      <div className="flex justify-between font-medium mt-2 bg-white rounded-full px-10">
        <NavLink
          to="/nghi-phep/danh-sach-phep"
          activeclass="active"
          className={`border-b-2 py-2 ${
            tab === "danh-sach-phep"
              ? "border-sky-400 text-sky-500"
              : "border-transparent"
          }`}
        >
          Danh sách phép
        </NavLink>
        <NavLink
          to="/nghi-phep/xin-nghi-phep"
          activeclass="active"
          className={`border-b-2 py-2 ${
            tab === "xin-nghi-phep"
              ? "border-sky-400 text-sky-500"
              : "border-transparent"
          }`}
        >
          Xin nghỉ phép
        </NavLink>
      </div>
      <div className="my-4">
        {tab === "xin-nghi-phep" && <XinNghiPhep />}
        {tab === "danh-sach-phep" && <DanhSachPhep />}
      </div>
    </div>
  );
}
