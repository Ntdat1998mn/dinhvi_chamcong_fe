import React, { useEffect, useRef } from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
import { userService } from "../../services/userService";
import { showMessage } from "../../utils/commonHandler";
import { nghiPhepService } from "../../services/nghiPhepService";
import { checkEmpty } from "../../utils/validate";

export default function XinNghiPhep() {
  let [startTime, setStartTime] = useState(1);
  let [endTime, setEndTime] = useState(2);
  const user = useSelector((state) => state.userSlice.userInfor);

  const [aproverList, setApoverList] = useState([]);
  const [leaderList, setLeaderList] = useState([]);
  const renderOptionAprover = (userList) => {
    return userList.map((userList, index) => {
      return (
        <option key={index} value={userList.nv_id}>
          {userList.nv_name}
        </option>
      );
    });
  };
  const approverRef = useRef(null);
  const leaderRef = useRef(null);
  const reasonRef = useRef(null);
  const startDateRef = useRef(null);
  const endDateRef = useRef(null);

  const handleFormSubmit = () => {
    const formInput = {
      ngay_bat_dau: startDateRef.current.value,
      ngay_ket_thuc: endDateRef.current.value,
      ca_bat_dau: startTime,
      ca_ket_thuc: endTime,
      ly_do: reasonRef.current.value,
      quan_ly_truc_tiep: leaderRef.current.value,
      nguoi_duyet: approverRef.current.value,
    };

    for (let key in formInput) {
      if (!checkEmpty(formInput[key])) {
        showMessage("Vui lòng nhập đầy đủ thông tin!", "error");
        return;
      }
    }
    nghiPhepService
      .xinNghiPhep(formInput, user.token)
      .then((res) => {
        showMessage(res.data.message, "success");
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  };

  useEffect(() => {
    userService
      .layNguoiDungBanLanhDaoCongTy(user.token)
      .then((res) => {
        setApoverList(res.data.content);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
    userService
      .layNguoiDungCungPhongBan(user.danhmuc_id, user.token)
      .then((res) => {
        setLeaderList(res.data.content);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  }, []);
  return (
    <div className="content bg-white px-4 py-6">
      {/* <div className="mb-4">
        <p className="mb-2">Số ngày nghỉ phép:</p>
        <div className="flex w-full">
          <button
            onClick={() => {
              setNumberDayOff("one");
            }}
            className="flex justify-center w-1/2 items-center"
          >
            <div className="bg-white w-4 h-4 border border-gray-400 rounded-full flex items-center justify-center">
              {numberDayOff === "one" && (
                <div className="bg-blue-500 w-2 h-2 rounded-full"></div>
              )}
            </div>
            <span className="ml-2">1 ngày</span>
          </button>
          <button
            onClick={() => {
              setNumberDayOff("many");
            }}
            className="flex justify-center w-1/2 items-center"
          >
            <div className="bg-white w-4 h-4 border border-gray-400 rounded-full flex items-center justify-center">
              {numberDayOff === "many" && (
                <div className="bg-blue-500 w-2 h-2 rounded-full"></div>
              )}
            </div>
            <span className="ml-2">nhiều ngày</span>
          </button>
        </div>
      </div> */}
      <div className="mb-4">
        <div className="mb-2">
          Bắt đầu:
          <span className="ml-2">
            <input
              ref={startDateRef}
              className="outline-1 outline-gray-400 outline rounded px-2 focus:outline-sky-500"
              type="date"
            />
          </span>
        </div>
        <div className="flex w-full">
          <button
            onClick={() => {
              setStartTime(1);
            }}
            className="flex justify-center w-1/2 items-center"
          >
            <div className="bg-white w-4 h-4 border border-gray-400 rounded-full flex items-center justify-center">
              {startTime === 1 && (
                <div className="bg-blue-500 w-2 h-2 rounded-full"></div>
              )}
            </div>
            <span className="ml-2">Buổi sáng</span>
          </button>
          <button
            onClick={() => {
              setStartTime(2);
            }}
            className="flex justify-center w-1/2 items-center"
          >
            <div className="bg-white w-4 h-4 border border-gray-400 rounded-full flex items-center justify-center">
              {startTime === 2 && (
                <div className="bg-blue-500 w-2 h-2 rounded-full"></div>
              )}
            </div>
            <span className="ml-2">Buổi chiều</span>
          </button>
        </div>
      </div>
      <div className="mb-4">
        <div className="mb-2">
          Kết thúc:
          <span className="ml-2">
            <input
              ref={endDateRef}
              className="outline-1 outline-gray-400 outline rounded px-2 focus:outline-sky-500"
              type="date"
            />
          </span>
        </div>
        <div className="flex w-full">
          <button
            onClick={() => {
              setEndTime(1);
            }}
            className="flex justify-center w-1/2 items-center"
          >
            <div className="bg-white w-4 h-4 border border-gray-400 rounded-full flex items-center justify-center">
              {endTime === 1 && (
                <div className="bg-blue-500 w-2 h-2 rounded-full"></div>
              )}
            </div>
            <span className="ml-2">Buổi sáng</span>
          </button>
          <button
            onClick={() => {
              setEndTime(2);
            }}
            className="flex justify-center w-1/2 items-center"
          >
            <div className="bg-white w-4 h-4 border border-gray-400 rounded-full flex items-center justify-center">
              {endTime === 2 && (
                <div className="bg-blue-500 w-2 h-2 rounded-full"></div>
              )}
            </div>
            <span className="ml-2">Buổi chiều</span>
          </button>
        </div>
      </div>
      <div className="mb-4">
        <div className="mb-2">
          Quản lý trực tiếp:
          <span className="ml-2">
            <select
              ref={leaderRef}
              className="outline-1 outline-gray-400 outline rounded px-2 focus:outline-sky-500"
            >
              <option value="">---Chọn---</option>
              {renderOptionAprover(leaderList)}
            </select>
          </span>
        </div>
      </div>
      <div className="mb-4">
        <div className="mb-2">
          Người duyệt:
          <span className="ml-2">
            <select
              ref={approverRef}
              className="outline-1 outline-gray-400 outline rounded px-2 focus:outline-sky-500"
            >
              <option value="">---Chọn---</option>
              {renderOptionAprover(aproverList)}
            </select>
          </span>
        </div>
      </div>
      <div>
        <p className="mb-2">Lý do:</p>
        <textarea
          ref={reasonRef}
          rows="4"
          className="w-full outline outline-1 outline-gray-400 rounded"
        ></textarea>
      </div>
      <button
        onClick={handleFormSubmit}
        className="text-white rounded-full mt-6 py-1 w-full bg-gradient-to-b from-sky-400 to-blue-900"
      >
        GỬI YÊU CẦU
      </button>
    </div>
  );
}
