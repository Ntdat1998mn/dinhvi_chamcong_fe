import React from "react";
import { useParams } from "react-router-dom";
import OutTimeScheduleList from "./OutTimeScheduleList";
import RegisterSchedule from "./RegisterSchedule";
import { NavLink } from "react-router-dom";

export default function OutTimePage() {
  const { tab } = useParams();

  return (
    <div className="text-gray-500">
      {/* Phần điều hướng */}
      <div className="flex justify-between font-medium mt-2 bg-white rounded-full px-10">
        <NavLink
          to="/tang-ca/lich-tang-ca"
          activeclass="active"
          className={`border-b-2 py-2 ${
            tab == "lich-tang-ca"
              ? "border-sky-400 text-sky-500"
              : "border-transparent"
          }`}
        >
          Lịch tăng ca
        </NavLink>
        <NavLink
          to="/tang-ca/dang-ky"
          activeclass="active"
          className={`border-b-2 py-2 ${
            tab == "dang-ky"
              ? "border-sky-400 text-sky-500"
              : "border-transparent"
          }`}
        >
          Đăng ký
        </NavLink>
      </div>
      {/* Phần nội dung */}
      <div className="my-4">
        {tab == "lich-tang-ca" ? <OutTimeScheduleList /> : <RegisterSchedule />}
      </div>
    </div>
  );
}
