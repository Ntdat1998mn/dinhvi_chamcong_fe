import React, { useEffect } from "react";
import { useState } from "react";
import { circleSVG } from "../../issets/img/svg";
import { lichTangCaService } from "../../services/lichTangCaService";
import { showMessage } from "../../utils/commonHandler";
import { useSelector } from "react-redux";

export default function OutTimeScheduleList() {
  const user = useSelector((state) => state.userSlice.userInfor);
  const [outTimeScheduleList, setOutTimeScheduleList] = useState([]);
  const huyDonXinTangCa = (id) => {
    lichTangCaService
      .huyDonTangCa(id, user.token)
      .then((res) => {
        showMessage(res.data.message, "success");
        setTimeout(() => {
          window.location.reload();
        }, 2000);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  };

  const renderOutTimeScheduleList = () => {
    return outTimeScheduleList.map((schedule, index) => {
      return (
        <div key={index} className="bg-white rounded-lg py-3 pl-6 mb-4">
          <div className="px-6 relative border-dashed border-blue-500 border-l-2">
            <p>
              <span>Vào ca: </span>
              <span>{schedule.thoi_gian_bat_dau}</span>
            </p>
            <p className="mt-2">
              <span>Ra ca: </span>
              <span>{schedule.thoi_gian_ket_thuc}</span>
            </p>
            <p className="mt-2">
              <span>Từ ngày: </span>
              <span>{schedule.ngay_bat_dau}</span>
            </p>
            <p className="mt-2">
              <span>Đến ngày: </span>
              <span>{schedule.ngay_ket_thuc}</span>
            </p>
            <p className="mt-2">
              <span>Quản lý trực tiếp: </span>
              <span className="font-medium text-lime-500">
                {schedule.thong_tin_nguoi_quan_ly_truc_tiep.nv_name}
              </span>
            </p>
            <p className="mt-2">
              <span>Người duyệt: </span>
              <span className="font-medium text-lime-500">
                {schedule.thong_tin_nguoi_duyet.nv_name}
              </span>
            </p>
            <p className="mt-2">
              <span>Trạng thái: </span>
              <span>
                {schedule.status === 1 ? (
                  <span className="font-medium text-red-500">Chưa duyệt</span>
                ) : schedule.status === 2 ? (
                  <span className="font-medium text-red-500">
                    Quản lý trực tiếp đã duyệt
                  </span>
                ) : schedule.status === 3 ? (
                  <span className="font-medium text-lime-500">Đã duyệt</span>
                ) : schedule.status === 4 ? (
                  <span className="font-medium text-gray-500">Quá hạn</span>
                ) : (
                  <span className="font-medium text-gray-500">
                    Không xác định
                  </span>
                )}
              </span>
            </p>

            <span className="absolute -margin-left-1 -top-1 -left-0 -translate-x-1/2 fill-sky-400 bg-white">
              {circleSVG}
            </span>
            <span className="absolute -margin-left-1 bottom-0 left-0 -translate-x-1/2 fill-gray-400">
              {circleSVG}
            </span>
            {schedule.status !== 4 && schedule.status !== 3 && (
              <button
                onClick={() => {
                  huyDonXinTangCa(schedule.id);
                }}
                className="bg-red-500 text-white w-full mt-2 active:bg-gray-500"
              >
                Huỷ
              </button>
            )}
          </div>
        </div>
      );
    });
  };
  useEffect(() => {
    lichTangCaService
      .getLichTangCaByUserId(user.token)
      .then((res) => {
        setOutTimeScheduleList(res.data.content);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  }, []);
  return <div className="content px-4">{renderOutTimeScheduleList()}</div>;
}
