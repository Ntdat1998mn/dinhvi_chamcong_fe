import React, { useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { userService } from "../../services/userService";
import { showMessage } from "../../utils/commonHandler";
import { checkEmpty } from "../../utils/validate";
import { lichTangCaService } from "../../services/lichTangCaService";

export default function RegisterSchedule() {
  const user = useSelector((state) => state.userSlice.userInfor);

  const [aproverList, setApoverList] = useState([]);
  const [leaderList, setLeaderList] = useState([]);
  const renderOptionAprover = (userList) => {
    return userList.map((userList, index) => {
      return (
        <option key={index} value={userList.nv_id}>
          {userList.nv_name}
        </option>
      );
    });
  };
  const startDateRef = useRef(null);
  const endDateRef = useRef(null);
  const startTimeRef = useRef(null);
  const endTimeRef = useRef(null);
  const approverRef = useRef(null);
  const leaderRef = useRef(null);
  const resetForm = () => {
    startDateRef.current.value = "";
    endDateRef.current.value = "";
    startTimeRef.current.value = "";
    endTimeRef.current.value = "";
    approverRef.current.value = "";
    leaderRef.current.value = "";
  };
  const handleFormSubmit = () => {
    const formInput = {
      ngay_bat_dau: startDateRef.current.value,
      ngay_ket_thuc: endDateRef.current.value,
      thoi_gian_bat_dau: startTimeRef.current.value,
      thoi_gian_ket_thuc: endTimeRef.current.value,
      quan_ly_truc_tiep: leaderRef.current.value,
      nguoi_duyet: approverRef.current.value,
    };
    for (let key in formInput) {
      if (!checkEmpty(formInput[key])) {
        showMessage("Vui lòng nhập đầy đủ thông tin!", "error");
        return;
      }
    }
    lichTangCaService
      .dangKyLichTangCa(formInput, user.token)
      .then((res) => {
        showMessage(res.data.message, "success");
        resetForm();
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  };

  useEffect(() => {
    userService
      .layNguoiDungBanLanhDaoCongTy(user.token)
      .then((res) => {
        setApoverList(res.data.content);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
    userService
      .layNguoiDungCungPhongBan(user.danhmuc_id, user.token)
      .then((res) => {
        setLeaderList(res.data.content);
      })
      .catch((err) => {
        showMessage(err.response.data.message, "error");
      });
  }, []);
  return (
    <div>
      <p className="mb-2">Từ ngày: </p>
      <input
        ref={startDateRef}
        type="date"
        className="mb-4 outline-1 outline-blue-500 rounded-lg py-1 px-3 w-full"
      />

      <p className="mb-2">Đến ngày: </p>
      <input
        ref={endDateRef}
        type="date"
        className="mb-4 outline-1 outline-blue-500 rounded-lg py-1 px-3 w-full"
      />
      <p className="mb-2">Thời gian bắt đầu: </p>
      <input
        ref={startTimeRef}
        type="time"
        className="mb-4 outline-1 outline-blue-500 rounded-lg py-1 px-3 w-full"
      />
      <p className="mb-2">Thời gian kết thúc: </p>
      <input
        ref={endTimeRef}
        type="time"
        className="mb-4 outline-1 outline-blue-500 rounded-lg py-1 px-3 w-full"
      />
      <p className="mb-2">Quản lý trực tiếp: </p>
      <select
        ref={leaderRef}
        className="mb-4 outline-1 outline-blue-500 rounded-lg py-1 px-3 w-full"
      >
        <option value="">---Chọn---</option>
        {renderOptionAprover(leaderList)}
      </select>
      <p className="mb-2">Người duyệt: </p>
      <select
        ref={approverRef}
        className="mb-4 outline-1 outline-blue-500 rounded-lg py-1 px-3 w-full"
      >
        <option value="">---Chọn---</option>
        {renderOptionAprover(aproverList)}
      </select>
      {/*       <p className="mb-2">Số giờ tăng ca: </p>
      <input
    
        type="number"
        className="outline-1 outline-blue-500 rounded-lg py-1 px-3 w-full"
      /> */}
      <button
        onClick={handleFormSubmit}
        className="text-white rounded-full mt-6 py-1 w-full bg-gradient-to-b from-sky-400 to-blue-900"
      >
        XÁC NHẬN
      </button>
    </div>
  );
}
