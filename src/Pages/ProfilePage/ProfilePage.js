import React, { useState } from "react";
import avatarDefalt from "../../issets/img/avatar.jpg";
import { useSelector } from "react-redux";
import { formatDateDDMMYYYY } from "../../utils/commonHandler";
import ChangeAvatarModel from "../../Component/Modal/ChangeAvatarModel";
import { BASE_URL } from "../../services/configURL";
import UpdateUserInforModal from "../../Component/Modal/UpdateUserInforModal";

export default function ProfilePage() {
  const user = useSelector((state) => state.userSlice.userInfor);
  let [showAvatarModal, setShowAvatarModal] = useState(false);
  let [showUpdateUserInforModal, setShowUpdateUserInforModal] = useState(false);

  return (
    <div className="text-gray-500">
      <div className="flex items-center p-4 bg-white mt-4 rounded-2xl">
        <div className="w-36 h-36 p-1 border-2 border-sky-500 rounded-full">
          <img
            onClick={() => {
              setShowAvatarModal(true);
            }}
            className="h-full w-full rounded-full object-cover"
            src={user.nv_avatar ? `${BASE_URL + user.nv_avatar}` : avatarDefalt}
            alt="Đây là avatar"
          />
        </div>
        <div className="text-center flex-1">
          <h3 className="font-medium text-2xl text-sky-500 uppercase">
            {user.nv_name}
          </h3>
          <p>
            Chức vụ:{" "}
            <span className="uppercase">{user.dvcc_chuc_vu?.chuc_vu_name}</span>
          </p>
          <p>
            Ngày vào làm: <span>{formatDateDDMMYYYY(user.nv_ngayvaolam)}</span>
          </p>
        </div>
      </div>
      <div className="w-full p-4">
        <div className="border-b border-sky-500">
          <p className="text-black">Số điện thoại:</p>
          <p className="text-lime-400">{user.nv_sdt_lienhe}</p>
        </div>
        <div className="border-b border-sky-500">
          <p className="text-black">Emai:</p>
          <p>{user.nv_email}</p>
        </div>
        <div className="border-b border-sky-500">
          <p className="text-black">Ngày sinh:</p>
          <p>{formatDateDDMMYYYY(user.nv_ngaysinh)}</p>
        </div>
        <div className="border-b border-sky-500">
          <p className="text-black">Địa chỉ liên hệ:</p>
          <p>{user.nv_diachitamtru}</p>
        </div>
        <div className="border-b border-sky-500">
          <p className="text-black">Địa chỉ thường trú:</p>
          <p>{user.nv_diachithuongtru}</p>
        </div>
        <button
          onClick={() => {
            setShowUpdateUserInforModal(true);
          }}
          className="text-white rounded-full mt-6 py-1 w-full bg-gradient-to-b from-sky-400 to-blue-900"
        >
          CẬP NHẬT
        </button>
      </div>
      {showAvatarModal && (
        <ChangeAvatarModel setShowAvatarModal={setShowAvatarModal} />
      )}
      {showUpdateUserInforModal && (
        <UpdateUserInforModal
          setShowUpdateUserInforModal={setShowUpdateUserInforModal}
        />
      )}
    </div>
  );
}
