import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isValid: false,
  position: { latitude: 12.31926616927634, longitude: 108.40970181497218 },
};

export const locationSlice = createSlice({
  name: "location",
  initialState,
  reducers: {
    setIsValid: (state, action) => {
      state.isValid = action.payload;
    },
    setPosition: (state, action) => {
      state.position = action.payload;
    },
  },
});

export const { setIsValid, setPosition } = locationSlice.actions;

export default locationSlice.reducer;
