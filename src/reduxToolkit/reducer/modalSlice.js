import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isModal: false,
  isMenu: 0,
  isXinDiTreModal: false,
  isXinVeSomModal: false,
};

export const modalSlice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    setIsXinDiTreModal: (state, action) => {
      state.isModal = action.payload;
      state.isXinDiTreModal = action.payload;
    },
    setIsXinVeSomModal: (state, action) => {
      state.isModal = action.payload;
      state.isXinVeSomModal = action.payload;
    },
    setIsMenu: (state, action) => {
      state.isMenu = action.payload;
    },
  },
});

export const { setIsXinDiTreModal, setIsXinVeSomModal, setIsMenu } =
  modalSlice.actions;

export default modalSlice.reducer;
