import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  mainTab: "cham-cong",
};

export const tabSlice = createSlice({
  name: "tab",
  initialState,
  reducers: {
    setMainTab: (state, action) => {
      state.mainTab = action.payload;
    },
  },
});

export const { setMainTab } = tabSlice.actions;

export default tabSlice.reducer;
