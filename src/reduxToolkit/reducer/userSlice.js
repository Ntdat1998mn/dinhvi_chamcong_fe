import { createSlice } from "@reduxjs/toolkit";
import { sessionService } from "../../services/sessionService";
import { DINHVI_CHAMCONG_USER } from "../../services/configURL";

const initialState = {
  userInfor: sessionService.getItem(DINHVI_CHAMCONG_USER),
};

export const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUserInfor: (state, action) => {
      state.userInfor = { ...state.userInfor, ...action.payload };
      sessionService.setItem(state.userInfor, DINHVI_CHAMCONG_USER);
    },
  },
});

export const { setUserInfor } = userSlice.actions;

export default userSlice.reducer;
