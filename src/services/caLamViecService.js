import axios from "axios";
import { BASE_URL } from "./configURL";

export const caLamViecService = {
  // Lấy ldanh sách ca làm việc
  getCaLamViecList: (token) => {
    return axios({
      url:
        BASE_URL +
        "/api/dinh-vi-cham-cong/ca-lam-viec/lay-danh-sach-ca-lam-viec",
      method: "GET",
      headers: { token: token },
    });
  },
};
