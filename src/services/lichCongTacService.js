import axios from "axios";
import { BASE_URL } from "../services/configURL";

export const lichCongTacService = {
  // Lấy lịch công tác
  getLichCongTac: (token) => {
    return axios({
      url: BASE_URL + "/api/dinh-vi-cham-cong/lich-cong-tac/lay-lich-cong-tac",
      method: "GET",
      headers: { token: token },
    });
  },

  // Xin đi trễ
  dangKyLichCongTac: (data, token) => {
    return axios({
      url:
        BASE_URL + "/api/dinh-vi-cham-cong/lich-cong-tac/dang-ky-lich-cong-tac",
      method: "POST",
      data: data,
      headers: { token: token },
    });
  }, // Huỷ xin công tác
  huyDonXinCongTac: (id, token) => {
    return axios({
      url:
        BASE_URL +
        `/api/dinh-vi-cham-cong/lich-cong-tac/huy-lich-cong-tac/${id}`,
      method: "PUT",
      headers: { token: token },
    });
  },
};
