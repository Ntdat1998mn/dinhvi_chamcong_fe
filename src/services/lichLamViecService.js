import axios from "axios";
import { BASE_URL } from "../services/configURL";

export const lichLamViecService = {
  // Lấy lịch làm việc đăng ký
  getLichLamViecDangKy: (token) => {
    return axios({
      url:
        BASE_URL +
        "/api/dinh-vi-cham-cong/lich-lam-viec-dang-ky/lay-lich-lam-viec-dang-ky",
      method: "GET",
      headers: { token: token },
    });
  },
  // Lấy lịch làm việc được duyệt
  getLichLamViecDuocDuyet: (token) => {
    return axios({
      url:
        BASE_URL +
        "/api/dinh-vi-cham-cong/lich-lam-viec-duoc-duyet/lay-lich-lam-viec-duoc-duyet",
      method: "GET",
      headers: { token: token },
    });
  },

  // Xin đi trễ
  dangKyLichLamViec: (data, token) => {
    return axios({
      url:
        BASE_URL +
        "/api/dinh-vi-cham-cong/lich-lam-viec-dang-ky/dang-ky-lich-lam-viec",
      method: "POST",
      data: data,
      headers: { token: token },
    });
  },
};
