import axios from "axios";
import { BASE_URL } from "./configURL";

export const lichTangCaService = {
  // Lấy lịch tăng ca
  getLichTangCa: (token) => {
    return axios({
      url: BASE_URL + "/api/dinh-vi-cham-cong/lich-tang-ca/lay-lich-tang-ca",
      method: "GET",
      headers: { token: token },
    });
  },
  // Lấy lịch tăng ca theo người dùng
  getLichTangCaByUserId: (token) => {
    return axios({
      url:
        BASE_URL +
        "/api/dinh-vi-cham-cong/lich-tang-ca/lay-lich-tang-ca-theo-nguoi-dung",
      method: "GET",
      headers: { token: token },
    });
  },

  // Đăng ký tăng ca
  dangKyLichTangCa: (data, token) => {
    return axios({
      url:
        BASE_URL + "/api/dinh-vi-cham-cong/lich-tang-ca/dang-ky-lich-tang-ca",
      method: "POST",
      data: data,
      headers: { token: token },
    });
  },
  // Huỷ xin tăng ca
  huyDonTangCa: (id, token) => {
    return axios({
      url:
        BASE_URL +
        `/api/dinh-vi-cham-cong/lich-tang-ca/huy-don-xin-tang-ca/${id}`,
      method: "PUT",
      headers: { token: token },
    });
  },
};
