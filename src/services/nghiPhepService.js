import axios from "axios";
import { BASE_URL } from "../services/configURL";

export const nghiPhepService = {
  // Lấy nghỉ phép
  getNghiPhep: (token) => {
    return axios({
      url:
        BASE_URL +
        "/api/dinh-vi-cham-cong/phep-nghi/lay-lich-phep-nghi-theo-nhan-vien",
      method: "GET",
      headers: { token: token },
    });
  },
  // Xin nghỉ phép
  xinNghiPhep: (data, token) => {
    return axios({
      url: BASE_URL + "/api/dinh-vi-cham-cong/phep-nghi/xin-phep-nghi",
      method: "POST",
      data: data,
      headers: { token: token },
    });
  },
  // Huỷ xin nghỉ phép
  huyDonNghiPhep: (id, token) => {
    return axios({
      url: BASE_URL + `/api/dinh-vi-cham-cong/phep-nghi/huy-don-xin-phep/${id}`,
      method: "PUT",
      headers: { token: token },
    });
  },
};
