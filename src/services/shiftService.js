import axios from "axios";
import { BASE_URL } from "../services/configURL";

export const shiftService = {
  // Vào ca
  checkIn: (token) => {
    return axios({
      url: BASE_URL + "/api/dinh-vi-cham-cong/cham-cong/vao-ca",
      method: "POST",
      headers: { token: token },
    });
  },
  // Ra ca
  checkOut: (token) => {
    return axios({
      url: BASE_URL + "/api/dinh-vi-cham-cong/cham-cong/ra-ca",
      method: "POST",
      headers: { token: token },
    });
  },
  // Xin đi trễ
  xinDiTre: (data, token) => {
    return axios({
      url: BASE_URL + "/api/dinh-vi-cham-cong/xin-di-tre/xin-di-tre",
      method: "POST",
      data: data,
      headers: { token: token },
    });
  },
  // Xin về sớm
  xinVeSom: (data, token) => {
    return axios({
      url: BASE_URL + "/api/dinh-vi-cham-cong/xin-ve-som/xin-ve-som",
      method: "POST",
      data: data,
      headers: { token: token },
    });
  },
};
