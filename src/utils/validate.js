export const checkEmpty = (input) => {
  if (input === null || input === undefined || String(input).trim() === "") {
    return false;
  }
  return true;
};
